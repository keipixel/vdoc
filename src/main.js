import Vue from 'vue'
import router from './router'
import App from './App.vue'

import VUI from '../source'
Vue.use(VUI)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')