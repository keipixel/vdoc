import Vue from 'vue'
import Router from 'vue-router'
import Component from './components/Component'

Vue.use(Router)

// include markdown files from your source code
const docfile = (file) => {
  return require(`../source/docs/${file}`).default
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: docfile('home.md')
    },
    {
      path: '/components',
      name: 'Component',
      component: Component,
      unlinked: true,
      children: [
        {
          path: 'button',
          name: 'Button',
          component: docfile('components/button.md')
        },
        {
          path: 'card',
          name: 'Card',
          component: docfile('components/card.md')
        }
      ]
    },
    {
      path: '/about',
      name: 'About',
      component: docfile('about.md')
    }
  ]
})
