# Button

## Example

```html
<template>
  <v-button
    @click='handleClick'
    class='button-demo'
  >
    click me
  </v-button>
</template>

<script>
export default {
  methods: {
    handleClick (e) {
      e.target.innerText = e.target.innerText === '=_='
        ? '>_<'
        : '=_='
    }
  }
}
</script>

<!-- button-demo.vue -->
```

## Property

|  name |   type   |
|-------|----------|
| click | Function |
