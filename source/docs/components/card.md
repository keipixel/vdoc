# Card

## Example

```html
<template>
  <v-card class='card-demo' title='HI Header'>
    <p>This is content</p>
    <v-button>ok</v-button>
  </v-card>
</template>

<style>
.card-demo {
  max-width: 50%;
}
</style>

<!-- card-demo.vue -->
```

## Property

|  name |  type  |
|-------|--------|
| title | String |