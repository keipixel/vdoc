import VButton from './components/button.vue'
import VCard from './components/card.vue'

const VUI = {
  install (Vue, options) {
    Vue.component(VButton.name, VButton)
    Vue.component(VCard.name, VCard)
  }
}

export default VUI